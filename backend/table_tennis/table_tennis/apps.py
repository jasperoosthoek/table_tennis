from django.apps import AppConfig


class TableTennisConfig(AppConfig):
    name = 'table_tennis'
