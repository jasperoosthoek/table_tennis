import React, { Component } from 'react';
import { Route } from 'react-router-dom';

import requireAuth from './RequireAuth';

const AuthenticatedRoute = ({ component: Component, ...restProps }) => {
    const AuthComponent = requireAuth(Component);

    return (
        <Route
            {...restProps}
            component={props => <AuthComponent {...props} />}
        />
    )
}

export default AuthenticatedRoute;
