import React, { Component } from 'react';
import Root from './Root';
import { Route, Switch } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import Login from './components/login/Login';

import Dashboard from './components/dashboard/Dashboard';
import AuthenticatedRoute from './utils/AuthenticatedRoute';

class App extends Component {
  render() {
    return (
      <div>
        <Root>
          <ToastContainer hideProgressBar={true} newestOnTop={true} />
          <Switch>
            <Route path='/login' component={Login} />
            <AuthenticatedRoute exact path='/' component={Dashboard} />
          </Switch>
        </Root>
      </div>
    );
  }
}

export default App;