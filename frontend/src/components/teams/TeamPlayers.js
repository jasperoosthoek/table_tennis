
import React, { Component, useContext } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Badge } from 'react-bootstrap';

import { mapPlayersToProps } from '../../redux/reducers/Players';

const TeamPlayers = ({ team, playersList, component, separator }) => team.players.map((playerId, key) => {
  const Component = component || Badge;
  return (
    <span key={key}>
      <Component variant="primary" style={{ cursor: 'default' }}>
        {playersList[playerId]
          ? playersList[playerId].name
          : <i>Not found</i>
        }
      </Component>
    {key < team.players.length - 1 && (separator || <>&nbsp;</>)}
  </span>)
});


export default connect(mapPlayersToProps)(TeamPlayers);