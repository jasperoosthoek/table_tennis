import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Row, Col, Navbar, Nav, Tabs, Tab } from 'react-bootstrap';
import _ from 'lodash';
import Websocket from 'react-websocket';
import { toast } from 'react-toastify';

import playersActions from '../../redux/actions/Players';
import teamsActions from '../../redux/actions/Teams';
import gamesActions from '../../redux/actions/Games';
import { mapPlayersToProps } from '../../redux/reducers/Players';
import { mapTeamsToProps } from '../../redux/reducers/Teams';
import { mapGamesToProps } from '../../redux/reducers/Games';

import { logout } from '../../redux/actions/Login';
import LoadingIndicator from '../shared/LoadingIndicator';
import PlayersList from '../players/PlayersList';
import TeamsList from '../teams/TeamsList';
import GamesList from '../games/GamesList';
import Points from '../points/Points';

class Dashboard extends Component {

  state = {
    activeTab: 'players',  }

  componentDidMount() {
    this.props.getPlayersList();
    this.props.getTeamsList();
    this.props.getGamesList();
  }

  render() {
    const { user } = this.props.auth;
    const { playersList, teamsList, gamesList } = this.props;
    return (
      <div>
        <Navbar bg='light'>
          <Navbar.Brand href='/'>Table Tennis Game</Navbar.Brand>
          <Navbar.Collapse className='justify-content-end'>
            <Navbar.Text>
              User: <b>{user.username}</b>
            </Navbar.Text>
            <Nav.Link onClick={() => this.props.logout()}>Logout</Nav.Link>
          </Navbar.Collapse>
        </Navbar>
        <Tabs
          activeKey={this.state.activeTab}
          onSelect={tab => this.setState({ activeTab: tab })}
          className="mb-3"
        >
          <Tab
            eventKey="players"
            title="Players"
          >
            <PlayersList />
          </Tab>
          <Tab
            eventKey="teams"
            title="Teams"
            disabled={!playersList || Object.keys(playersList).length === 0}
          >
            <TeamsList />
          </Tab>
          <Tab
            eventKey="games"
            title="Games"
            disabled={!teamsList || Object.keys(teamsList).length < 2}
          >
            <GamesList />
          </Tab>
        </Tabs>
      </div>
    );
  }
}

Dashboard.propTypes = {
  logout: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = (state, ownState) => ({
  auth: state.auth,
  ...mapPlayersToProps(state),
  ...mapTeamsToProps(state),
  ...mapGamesToProps(state),
});

export default connect(mapStateToProps, {
  logout,
  ...playersActions,
  ...teamsActions,
  ...gamesActions,
})(withRouter(Dashboard));
