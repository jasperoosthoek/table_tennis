import React, { Component, useContext } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { ListGroup, Card, Container, Row } from 'react-bootstrap';

import LoadingIndicator from '../shared/LoadingIndicator';
import CreateEditModal from '../shared/CreateEditModal';
import { CreateButton, DeleteConfirmButton } from '../shared/IconButtons';
import { mapPlayersToProps } from '../../redux/reducers/Players';
import playerActions from '../../redux/actions/Players';

class PlayersList extends Component {
  state = {
    showNewModal: false

  }

  render() {
    const { getPlayersIsLoading, playersList, createPlayer, createPlayerIsLoading, updatePlayer, updatePlayerIsLoading, deletePlayer, deletePlayerIsLoading } = this.props;
    
    if (getPlayersIsLoading || !playersList) return <LoadingIndicator />;
    
    const formFields = { name: { label: 'Name' } };

    return <>
      <Container fluid className="App py-2 overflow-hidden">
        <Row className="card-example d-flex flex-row overflow-auto">
          {Object.values(playersList).map((player, key) =>
            <Card style={{ minWidth: '25rem', margin: '10px' }} key={key}>
              <Card.Body
                onClick={() => this.setState({ playerInEditModal: player })}
              >
                {player.name}
                <DeleteConfirmButton
                  modalTitle="Delete player"
                  onDelete={() => deletePlayer(player)}
                  loading={deletePlayerIsLoading}
                  className="float-right"
                />

              </Card.Body>
            </Card>
          )}
          <Card style={{ margin: '10px' }}>
            <Card.Body>
              <CreateButton onClick={() => this.setState({ showNewModal: true })} />
            </Card.Body>
          </Card>
        </Row>
      </Container>

      {this.state.playerInEditModal &&
        <CreateEditModal
          show={!!this.state.playerInEditModal}
          modalTitle="Modify player"
          loading={updatePlayerIsLoading}
          onHide={() => this.setState({ playerInEditModal: null })}
          initialState={this.state.playerInEditModal}
          formFields={formFields}

          onSave={player => this.props.updatePlayer(
            player,
            { callback: () => this.setState({ playerInEditModal: null }) }
          )}
      />}
      
      <CreateEditModal
        show={this.state.showNewModal}
        modalTitle="New player"
        loading={createPlayerIsLoading}
        onHide={() => this.setState({ showNewModal: false })}
        initialState={{ name: '' }}
        formFields={formFields}

        onSave={newPlayer => this.props.createPlayer(
          newPlayer,
          { callback: () => this.setState({ showNewModal: false }) }
        )}
      />
    </>;
  }
}

const mapStateToProps = (state, ownProps) => ({
});

export default connect(mapPlayersToProps, {
  ...playerActions,
})(PlayersList);