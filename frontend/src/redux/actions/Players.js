import axios from '../../utils/Axios';
import { toastOnError } from '../../utils/Utils';
import reduxCrudFactory from '../../utils/ReduxCrudFactory'

export const playersFactory = reduxCrudFactory(
    'players',
    {
        route: '/api/game/players/',
        axios,
        onError: toastOnError,
        actions: {
            getList: true,
            create: true,
            update: true,
            delete: true,
        },
    }
);

export default playersFactory.actions;