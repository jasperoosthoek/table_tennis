import axios from '../../utils/Axios';
import { toastOnError } from '../../utils/Utils';
import reduxCrudFactory from '../../utils/ReduxCrudFactory'

export const pointsFactory = reduxCrudFactory(
    'points',
    {
        route: '/api/game/games/points/',
        axios,
        onError: toastOnError,
        id: 'points',
        actions: {
            getList: true,
        },
    }
);

export default pointsFactory.actions;