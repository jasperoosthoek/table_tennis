import axios from '../../utils/Axios';
import { toastOnError } from '../../utils/Utils';
import reduxCrudFactory from '../../utils/ReduxCrudFactory'

export const gamesFactory = reduxCrudFactory(
    'games',
    {
        route: '/api/game/games/',
        axios,
        onError: toastOnError,
        actions: {
            getList: true,
            create: true,
            update: true,
            delete: true,
        },
        includeActions: {
            matchMaker: {
                isAsync: true,
                route: `/api/game/games/match_maker/`,
                method: 'post',
                onResponse: ({ create: createAction, set: setAction, ...rest }, games) => games.map(game => setAction(game)),
            }
        }
    }
);

export default gamesFactory.actions;
