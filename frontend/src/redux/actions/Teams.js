import axios from '../../utils/Axios';
import { toastOnError } from '../../utils/Utils';
import reduxCrudFactory from '../../utils/ReduxCrudFactory'

export const teamsFactory = reduxCrudFactory(
    'teams',
    {
        route: '/api/game/teams/',
        axios,
        onError: toastOnError,
        actions: {
            getList: true,
            create: true,
            update: true,
            delete: true,
        },
    }
);

export default teamsFactory.actions;