import { teamsFactory } from '../actions/Teams';

export default teamsFactory.reducerAsObject

export const mapTeamsToProps = teamsFactory.mapStateToProps;
