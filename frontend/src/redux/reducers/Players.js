import { playersFactory } from '../actions/Players';

export default playersFactory.reducerAsObject

export const mapPlayersToProps = playersFactory.mapStateToProps;
