import { pointsFactory } from '../actions/Points';

export default pointsFactory.reducerAsObject

export const mapPointsToProps = pointsFactory.mapStateToProps;
