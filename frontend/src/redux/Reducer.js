import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

import { LOGIN_UNSET_CURRENT_USER } from './actions/Login';
import { loginReducer } from './reducers/Login';
import playersReducer from './reducers/Players';
import teamsReducer from './reducers/Teams';
import gamesReducer from './reducers/Games';
import pointsReducer from './reducers/Points';

const consoleLogReducer = (state = null, { type, ...action }) => {
    console.log(type, action, state);
    return state;
}

export default history => {
    const router = connectRouter(history);

    const rootReducer = (state, action) => consoleLogReducer(combineReducers({
        router,
        auth: loginReducer,
        ...playersReducer,
        ...teamsReducer,
        ...gamesReducer,
        ...pointsReducer,
    })(state, action), action);
    
    return (state, action) => {
        if (action.type === LOGIN_UNSET_CURRENT_USER) {
            return rootReducer(undefined, action)
        }

        return rootReducer(state, action)
    };
}

